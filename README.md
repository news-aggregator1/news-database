##News database
Project containing liquibase migrations configured to use with PostgreSQL db.

###RUN MIGRATIONS
1. Set environment variables:
   - DB_ADDRESS - database host address,
   - DB_PORT - database port,
   - DB_NAME - database name,
   - DB_USERNAME - database account username,
   - DB_PASSWORD - database account password,

2. Run ./mvnw liquibase:update in cmd.
